Instructions in order to run the "Expense-Manager" correctly:

The app data will be stored in a mySQL database, so you need to set up
a mySQL database and execute the sql queries in sql/createTables.sql
to create the two tables for the expense-manager. Next you have to
change the WEB-INF/classes/sql.properties file to your mySQL login settings.
Then you have to modify the build.properties file (change to your tomcat
location and login), so that deployment over ant can work.
Finally you can deploy the app with:
ant deploy

