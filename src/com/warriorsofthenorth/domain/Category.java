package com.warriorsofthenorth.domain;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */
public class Category {

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
