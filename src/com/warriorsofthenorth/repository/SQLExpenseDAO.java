package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;
import org.apache.commons.logging.LogFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */


@ManagedBean(name="expenseDAO", eager=true)
@SessionScoped
public class SQLExpenseDAO implements ExpenseDAO {

    protected final org.apache.commons.logging.Log logger = LogFactory.getLog(getClass());

    private Connection getConnection()
            throws SQLException {
        Properties props = new Properties();
        props = getSqlProperties(props);

        try{
            Class.forName(props.getProperty("jdbcDriver"));
        } catch (ClassNotFoundException e) {
            logger.error("JDBC driver not found" + e);
        }


        String connectURL = "jdbc:mysql://" + props.getProperty("dbHost") + ":"
                + props.getProperty("dbPort") + "/" + props.getProperty("dbName");
        Connection conn = DriverManager.getConnection(connectURL, props);
        return conn;
    }


    private Properties getSqlProperties(Properties properties){
        try{
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("sql.properties"));

        }catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }


    @Override
    public void createExpense(Expense expense) {
        PreparedStatement stmnt = null;
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String createSQL = "INSERT INTO expenses(description, category, amount, adate) " +
                "values(?,?,?,?)";

        try{
            Connection con = this.getConnection();
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(createSQL);
            stmnt.setString(1,expense.getDescription());
            stmnt.setString(2, expense.getCategory().getCategory());
            stmnt.setString(3, Double.toString(expense.getAmount()));
            stmnt.setString(4, dateFormater.format(expense.getDate()));
            stmnt.executeUpdate();
            con.commit();
            stmnt.close();
        }catch(SQLException e){
            System.out.print("SQL Error: " + e);
        }
    }

    @Override
    public List<Expense> readAllExpenses() {
        List<Expense> expenseSet = new LinkedList<Expense>();
        Expense expense;

        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM expenses ";

        try{
            Connection con = this.getConnection();
            stmnt = con.prepareStatement(readSQL);
            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return expenseSet;

            while(results.next()){
                expense = new Expense();
                Category category = new Category();
                category.setCategory( results.getString( "category" ) );
                expense.setCategory(category);
                expense.setAmount( Double.parseDouble( results.getString( "amount" ) ) );
                expense.setDescription( results.getString( "description" ) );
                expense.setDate(dateFormater.parse(results.getString("adate")));
                expenseSet.add(expense);
            }
            stmnt.close();

        }catch(SQLException e){
            logger.error("SQL Except: " + e);
            System.out.print("SQL Error: " + e);
        } catch (ParseException e) {
            System.out.println("Parser Except: " + e);
        }

        return expenseSet;
    }

    @Override
    public List<Expense> readExpensesPerCategory(Category filterCategory) {
        List<Expense> expenseSet = new LinkedList<Expense>();
        Expense expense;
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM expenses ";

        try{
            Connection con = this.getConnection();
            stmnt = con.prepareStatement(readSQL);
            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return expenseSet;

            while(results.next()){
                expense = new Expense();
                Category category = new Category();
                category.setCategory( results.getString( "category" ) );
                expense.setCategory(category);
                expense.setAmount( Double.parseDouble( results.getString( "amount" ) ) );
                expense.setDescription( results.getString( "description" ) );
                expense.setDate(dateFormater.parse(results.getString("adate")));

                if(category.getCategory().equals(filterCategory.getCategory())){
                    expenseSet.add(expense);
                }
            }
            stmnt.close();

        }catch(SQLException e){
            System.out.print("SQL Error: " + e);
        } catch (ParseException e) {
            System.out.println("Parser Except: " + e);
        }

        return expenseSet;
    }

    @Override
    public void deleteExpensesOfCategory(Category category) {
        PreparedStatement stmnt = null;
        String deleteSQL = "DELETE FROM expenses WHERE category = ?";
        try{
            Connection con = this.getConnection();
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(deleteSQL);
            stmnt.setString(1, category.getCategory());
            stmnt.executeUpdate();
            con.commit();
            stmnt.close();

        }catch(SQLException e){
            System.out.print("SQL Error: " + e);
        }
    }
}
