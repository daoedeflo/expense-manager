package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;

import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */
public interface CategoryDAO {
    void addCategory(Category category);
    Set<Category> readAllCategories();
    void deleteCategory(Category category);
}
