package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;

import java.util.List;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */
public interface ExpenseDAO {
    void createExpense(Expense expense);
    List<Expense> readAllExpenses();
    List<Expense> readExpensesPerCategory(Category filterCategory);
    void deleteExpensesOfCategory(Category category);
}
