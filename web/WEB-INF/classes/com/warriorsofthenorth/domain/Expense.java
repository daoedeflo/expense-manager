package com.warriorsofthenorth.domain;

import java.util.Date;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */
public class Expense {
    private String description;
    private Category category;
    private double amount;
    private Date date;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
