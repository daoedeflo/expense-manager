package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */

//@ManagedBean(name="categoryDAO", eager=true)
//@SessionScoped
public class XmlCategoryDAO implements CategoryDAO{

    private String categoryXMLName = "web/data/categories.xml";

    @Override
    public void addCategory(Category category) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        if(isCategoryAlreadyExisting(category)){
            return;
        }

        try {
            File file = new File(categoryXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(file,documentBuilder);

            Node root = document.getFirstChild();
            Element element = document.createElement("category");
            element.setTextContent(category.getCategory());
            root.appendChild(element);

            this.writeChangesToXMLFile(document);

        }catch(ParserConfigurationException e){
            System.out.println("ParserConfig Excep: " + e);
        }
    }

    @Override
    public Set<Category> readAllCategories() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Set<Category> categorySet = new HashSet<Category>();
        Category category;

        try{
            File file = new File(categoryXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(file, documentBuilder);

            document.getDocumentElement().normalize();
            NodeList categoriesList = document.getElementsByTagName("categories");

            for(int index = 0 ; index < categoriesList.getLength() ; index++ ){
                Node node = categoriesList.item(index);
                if(node.getNodeType() == Node.ELEMENT_NODE ){
                    Element element = (Element) node;
                    for(int i=0;i<element.getElementsByTagName("category").getLength();i++){
                        category = new Category();
                        category.setCategory(element.getElementsByTagName("category").item(i)
                                                                                    .getTextContent());
                        categorySet.add(category);
                    }
                }
            }

        }catch(ParserConfigurationException e) {
            System.out.println("ParserConfig Excep: " + e);
        }

        return categorySet;
    }

    @Override
    public void deleteCategory(Category category) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try{
            File file = new File(categoryXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document= returnExistingOrNewDocument(file,documentBuilder);

            Node root = document.getFirstChild();
            NodeList categoryList = document.getElementsByTagName("category");

            for(int i=0;i<categoryList.getLength();i++) {
                Node node = categoryList.item(i);
                if (node.getTextContent().equals(category.getCategory())) {
                    document.getFirstChild().removeChild(node);
                }
            }

            this.writeChangesToXMLFile(document);

        }catch(ParserConfigurationException e){
            System.out.println("ParserConfig Excep: " + e);
        }
    }

    private boolean isCategoryAlreadyExisting(Category category){
        Set<Category> categorySet = this.readAllCategories();
        for(Category categoryLoop : categorySet){
            if (categoryLoop.getCategory().equals(category.getCategory())){
                System.out.println("Category already exists!!");
                return true;
            }
        }
        return false;
    }

    private Document returnExistingOrNewDocument (File file,
                                                  DocumentBuilder documentBuilder){
        Document document=null;
        try{
            if(file.exists() && !file.isDirectory()){
                document = documentBuilder.parse(file);
            }else{
                this.createCategoriesFile();
                document = documentBuilder.parse(file);
            }
        }catch(SAXException e){
            System.out.println("SAX Excep: " + e);
        }catch(IOException e){
            System.out.println("IO Excep: " + e);
        }
        return document;
    }

    private void createCategoriesFile(){
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("categories");
            document.appendChild(rootElement);

            this.writeChangesToXMLFile(document);

        }catch(ParserConfigurationException e){
            System.out.println("ParserConfig Excep: " + e);
        }
    }


    private void writeChangesToXMLFile(Document document){
        try{
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(categoryXMLName);
            transformer.transform(source, result);

        }catch(TransformerException e){
            System.out.println("Transformer Excep: " + e);
        }
    }
}
