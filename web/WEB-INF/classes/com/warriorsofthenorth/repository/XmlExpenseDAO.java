package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.commons.logging.LogFactory;

/**
 * Created by Florian Mitterbauer on 2/3/15.
 */

//@ManagedBean(name="expenseDAO", eager=true)
//@SessionScoped
public class XmlExpenseDAO implements ExpenseDAO {

    protected final org.apache.commons.logging.Log logger = LogFactory.getLog(getClass());


    private DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    private static final String expensesXMLName = "expenses.xml";


    private enum EXPENSE_ATTRIBUTES{
        DESCRIPTION("description"),
        CATEGORY("category"),
        AMOUNT("amount"),
        DATE("date");

        private String name="";
        EXPENSE_ATTRIBUTES(String name){
            this.name = name;
        }

        String getName(){
            return name;
        }

    }


    @Override
    public void createExpense(Expense expense) {

        try {
            File file = new File(expensesXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(file, documentBuilder);

            Node root = document.getFirstChild();
            Element expenseNode = document.createElement("expense");

            Set<String> expenseValueSet = this.getExpenseValuesSet(expense);
            Iterator<String> expenseValueSetIterator = expenseValueSet.iterator();

            for(EXPENSE_ATTRIBUTES expenseAttribute : EXPENSE_ATTRIBUTES.values()){
                Element descriptionElement = document.createElement(expenseAttribute.getName());
                descriptionElement.setTextContent( expenseValueSetIterator.next() );
                expenseNode.appendChild(descriptionElement);
            }
            root.appendChild(expenseNode);

            this.writeChangesToXMLFile(document);

        }catch(ParserConfigurationException e){
            System.out.println("PerserConfig Excep: " + e);
        }
    }


    private Set<String> getExpenseValuesSet(Expense expense){

        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Set<String> expenseValueSet = new LinkedHashSet<String>();
        expenseValueSet.add(expense.getDescription());
        expenseValueSet.add(expense.getCategory().getCategory());
        expenseValueSet.add(Double.toString(expense.getAmount()));
        expenseValueSet.add(dateFormater.format(expense.getDate()));

        return expenseValueSet;
    }

    @Override
    public List<Expense> readAllExpenses() {
        List<Expense> expensesList = new LinkedList<Expense>();
        String scheissbaut = "Kein scheiss baut";

        try{
            File file = new File(expensesXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(file, documentBuilder);

            document.getDocumentElement().normalize();
            NodeList expensesNodeList = document.getElementsByTagName("expense");
            scheissbaut = "bum " + expensesNodeList.getLength();
            for(int index = 0 ; index < expensesNodeList.getLength() ; index++ ){
                Node node = expensesNodeList.item(index);
                if(node.getNodeType() == Node.ELEMENT_NODE ){
                    Element element = (Element) node;
                    expensesList.add(fillExpenseWithElementData(element));
                }
            }
        }catch(ParserConfigurationException e) {
            System.out.println("PerserConfig Excep: " + e);

        }/*finally {
            List <Expense> expenseList = new LinkedList<Expense>();
            Expense myexpense = new Expense();
            myexpense.setAmount(2.34);
            myexpense.setDate(new Date());
            myexpense.setDescription(scheissbaut);
            Category mycategory = new Category();
            mycategory.setCategory("Bier");
            myexpense.setCategory(mycategory);

            expenseList.add(myexpense);

            myexpense = new Expense();
            myexpense.setAmount(55.54);
            myexpense.setDate(new Date());
            myexpense.setDescription("Servus Sepp");
            mycategory = new Category();
            mycategory.setCategory("Wein");
            myexpense.setCategory(mycategory);

            expenseList.add(myexpense);

            return expenseList;
        }*/

        return expensesList;
    }

    private Expense fillExpenseWithElementData(Element element){
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Expense expense = new Expense();
        expense.setDescription(element.getElementsByTagName("description").item(0)
                .getTextContent());
        Category category = new Category();
        category.setCategory(element.getElementsByTagName("category").item(0).getTextContent());
        expense.setCategory(category);
        expense.setAmount(Double.parseDouble(element.getElementsByTagName("amount")
                .item(0).getTextContent()));
        try{
            expense.setDate( dateFormater.parse( element.getElementsByTagName("date")
                    .item(0).getTextContent() )  );
        }catch (ParseException e) {
            System.out.println("Wrong date Fomat in Expense xml!");
        }
        return expense;
    }

    @Override
    public List<Expense> readExpensesPerCategory(Category filterCategory) {
        List<Expense> expensesList = new LinkedList<Expense>();

        try{
            File file = new File(expensesXMLName);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(file, documentBuilder);

            document.getDocumentElement().normalize();
            NodeList expensesNodeList = document.getElementsByTagName("expense");

            for(int index = 0 ; index < expensesNodeList.getLength() ; index++ ){
                Node node = expensesNodeList.item(index);
                if(node.getNodeType() == Node.ELEMENT_NODE ){
                    Element element = (Element) node;
                    Expense expense = fillExpenseWithElementData(element);

                    if( expense.getCategory().getCategory().equals(filterCategory.getCategory()) ){
                        expensesList.add(expense);
                    }
                }
            }
        }catch(ParserConfigurationException e) {
            System.out.println("ParserConfig Excep: " + e);
        }

        return expensesList;
    }

    @Override
    public void deleteExpensesOfCategory(Category category) {
        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = returnExistingOrNewDocument(new File(expensesXMLName),documentBuilder);

            NodeList categoryList = document.getElementsByTagName("expense");

            for(int i=0;i<categoryList.getLength();i++) {
                Node node = categoryList.item(i);

                if(node.getNodeType() == Node.ELEMENT_NODE ) {
                    Element element = (Element) node;

                    if(element.getElementsByTagName("category").item(0).getTextContent()
                            .equals(category.getCategory())){
                        document.getFirstChild().removeChild(node);
                        this.writeChangesToXMLFile(document);
                        this.deleteExpensesOfCategory(category);
                    }
                }
            }
        }catch(ParserConfigurationException e){
            System.out.println("ParserConfig Excep: " + e);
        }
    }


    private Document returnExistingOrNewDocument (File file, DocumentBuilder documentBuilder){
        Document document=null;
        try{
            if(file.exists() && !file.isDirectory()){
                document = documentBuilder.parse(file);
            }else{
                this.createExpensesFile();
                document = documentBuilder.parse(file);
            }
        }catch(SAXException e){
            System.out.println("SAX Excep: " + e);
        }catch(IOException e){
            logger.error("IO Exception: " + e);
            System.out.println("IO Excep: " + e);
        }
        return document;
    }

    private void createExpensesFile(){
        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("expenses");
            document.appendChild(rootElement);

            this.writeChangesToXMLFile(document);

        }catch(ParserConfigurationException e){
            logger.error("Parser in create IO Exception: " + e);
            System.out.println("ParserConfig Excep: " + e);
        }
    }


    private void writeChangesToXMLFile(Document document){
        try{
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(expensesXMLName);
            transformer.transform(source, result);

        }catch(TransformerException e){
            System.out.println("Transformer Excep: " + e);
        }
    }
}
