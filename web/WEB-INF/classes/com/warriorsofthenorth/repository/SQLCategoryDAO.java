package com.warriorsofthenorth.repository;

import com.warriorsofthenorth.domain.Category;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */


@ManagedBean(name="categoryDAO", eager=true)
@SessionScoped
public class SQLCategoryDAO implements CategoryDAO {

    private Connection getConnection()
            throws SQLException {

        Properties props = new Properties();
        props = getSqlProperties(props);

        try{
            Class.forName(props.getProperty("jdbcDriver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        String connectURL = "jdbc:mysql://" + props.getProperty("dbHost") + ":"
                + props.getProperty("dbPort") + "/" + props.getProperty("dbName");
        Connection conn = DriverManager.getConnection(connectURL, props);
        return conn;
    }

    private Properties getSqlProperties(Properties properties){
        try{
            properties.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("sql.properties"));

        }catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    @Override
    public void addCategory(Category category) {
        PreparedStatement stmnt = null;
        String createSQL = "INSERT INTO categories(category) values(?)";

        try{
            Connection con = this.getConnection();
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(createSQL);
            stmnt.setString(1, category.getCategory());
            stmnt.executeUpdate();
            con.commit();
            stmnt.close();
        }catch(SQLException e){
            System.out.print("SQL Error: " + e);
        }
    }

    @Override
    public Set<Category> readAllCategories() {

        Set<Category> categorySet = new LinkedHashSet<Category>();
        Category category;

        PreparedStatement stmnt = null;
        String readSQL="SELECT * FROM categories ";

        try{
            Connection con = this.getConnection();
            stmnt = con.prepareStatement(readSQL);
            ResultSet results = stmnt.executeQuery();

            if(!results.isBeforeFirst() && results.getRow() == 0)
                return categorySet;

            while(results.next()){
                category = new Category();
                category.setCategory(results.getString("category"));
                categorySet.add(category);
            }
            stmnt.close();

        }catch(Exception e){
            System.out.print("SQL Error: " + e);
        }

        return categorySet;
    }

    @Override
    public void deleteCategory(Category category) {
        PreparedStatement stmnt = null;
        String deleteSQL = "DELETE FROM categories WHERE category = ?";
        try{
            Connection con = this.getConnection();
            con.setAutoCommit(false);
            stmnt = con.prepareStatement(deleteSQL);
            stmnt.setString(1, category.getCategory());
            stmnt.executeUpdate();
            con.commit();
            stmnt.close();

        }catch(SQLException e){
            System.out.print("SQL Error: " + e);
        }
    }
}
