package com.warriorsofthenorth.service;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;

import java.util.List;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */
public interface ExpenseService {
    void addCategory(Category category);
    Set<Category> readAllCategories();
    void deleteCategory(Category category);
    void addExpense(Expense expense);
    List<Expense> readAllExpenses();
    List<Expense> readExpensesPerCategory(Category filterCategory);
}
