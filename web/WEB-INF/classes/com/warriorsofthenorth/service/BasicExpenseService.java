package com.warriorsofthenorth.service;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;
//import com.warriorsofthenorth.repository.BasicCategoryDAO;
//import com.warriorsofthenorth.repository.BasicExpenseDAO;
import com.warriorsofthenorth.repository.SQLCategoryDAO;
import com.warriorsofthenorth.repository.SQLExpenseDAO;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */

@ManagedBean(name="expenseService", eager=true)
@SessionScoped
public class BasicExpenseService implements ExpenseService {


    @ManagedProperty(value = "#{categoryDAO}")
    private SQLCategoryDAO categoryDAO;

    @ManagedProperty(value = "#{expenseDAO}")
    private SQLExpenseDAO expenseDAO;


    public SQLCategoryDAO getCategoryDAO() {
        return categoryDAO;
    }

    public void setCategoryDAO(SQLCategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    public SQLExpenseDAO getExpenseDAO() {
        return expenseDAO;
    }

    public void setExpenseDAO(SQLExpenseDAO expenseDAO) {
        this.expenseDAO = expenseDAO;
    }


    @Override
    public void addCategory(Category category) {
        if(category!=null && category.getCategory() != null){
            categoryDAO.addCategory(category);
        }
    }

    @Override
    public Set<Category> readAllCategories() {
        return categoryDAO.readAllCategories();
    }

    @Override
    public void deleteCategory(Category category) {
        categoryDAO.deleteCategory(category);
        expenseDAO.deleteExpensesOfCategory(category);
    }

    @Override
    public void addExpense(Expense expense) {
        if(expense!=null && expense.getDescription() != null && expense.getAmount() > 0.01d){
            expenseDAO.createExpense(expense);
        }
    }

    @Override
    public List<Expense> readAllExpenses() {

        return expenseDAO.readAllExpenses();
    }

    @Override
    public List<Expense> readExpensesPerCategory(Category filterCategory) {
        return expenseDAO.readExpensesPerCategory(filterCategory);
    }


}
