package com.warriorsofthenorth.web;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.service.BasicExpenseService;
import com.warriorsofthenorth.service.ExpenseService;
import org.apache.commons.logging.LogFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.Set;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */


@ManagedBean(name="duplicateCategoryValidator",eager=true)
@SessionScoped
public class DuplicateCategoryValidator implements Validator {


    @ManagedProperty(value="#{expenseService}")
    private ExpenseService expenseService;


    protected final org.apache.commons.logging.Log logger = LogFactory.getLog(getClass());


    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o)
            throws ValidatorException {
        Set<Category> allCategories = null;
        String newCategoryName = null;
        try{
            allCategories  = expenseService.readAllCategories();
        }catch(NullPointerException e){
            logger.error("injection not working");
        }

        try{
            newCategoryName = o.toString();
        }catch(NullPointerException e){
            logger.error("Object null in validator");
        }


        for(Category category : allCategories){
            if(category.getCategory().toLowerCase().equals(newCategoryName.toLowerCase())){
                FacesMessage errorMessage = new FacesMessage("Category already exists!");
                throw new ValidatorException(errorMessage);
            }
        }

    }


    public ExpenseService getExpenseService() {
        return expenseService;
    }

    public void setExpenseService(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }
}
