package com.warriorsofthenorth.web;

import com.warriorsofthenorth.domain.Category;
import com.warriorsofthenorth.domain.Expense;
import com.warriorsofthenorth.service.ExpenseService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.util.*;

/**
 * Created by Florian Mitterbauer on 2/8/15.
 */

@ManagedBean(name="expenseBean", eager=true)
@SessionScoped
public class ExpenseBean {

    @ManagedProperty(value="#{expenseService}")
    private ExpenseService expenseService;

    private String description;
    private String category;
    private double amount;


    public List<Expense> getExpenses(){

        return this.expenseService.readAllExpenses();
    }

    public void setExpense(){
        Expense expense = new Expense();
        expense.setDescription(this.description);
        Category mycategory = new Category();
        mycategory.setCategory(category);
        expense.setCategory(mycategory);
        expense.setDate(new Date());
        expense.setAmount(this.amount);
        this.expenseService.addExpense(expense);
    }


    private ArrayList<SelectItem> categories = null;


    public List<String> getCategoriesList(){
        List<String> categoryNameList = new LinkedList<String>();
        for(Category categoryLoop : this.expenseService.readAllCategories()){
            categoryNameList.add(categoryLoop.getCategory());
        }
        return categoryNameList;
    }

    public ArrayList<SelectItem> getCategories(){
        categories = new ArrayList<SelectItem>();
        categories.add(new SelectItem(null,"Select a Category"));
        for(Category categoryLoop : this.expenseService.readAllCategories()){
            categories.add(new SelectItem(categoryLoop.getCategory(),categoryLoop.getCategory()));
        }
        return categories;
    }


    public void addCategory(){
        Category mycategory = new Category();
        mycategory.setCategory(category);
        this.expenseService.addCategory(mycategory);
    }

    public void deleteCategory(){
        Category mycategory = new Category();
        mycategory.setCategory(category);
        this.expenseService.deleteCategory(mycategory);
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public ExpenseService getExpenseService() {
        return expenseService;
    }

    public void setExpenseService(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }
}
